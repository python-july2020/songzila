from django.urls import path
from apis import viewsets

urlpatterns = [
    path('contact/',viewsets.contact_api),
    path('songs/',viewsets.songs_api),
]