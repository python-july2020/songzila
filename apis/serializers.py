from myapp.models import Contact, Song, Category, Register_Table
from rest_framework import serializers
from django.shortcuts import get_object_or_404

class ContactSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False)
    class Meta:
        model = Contact 
        # fields = ['id','name','message']
        fields = ['id','name','email','message','subject']

    def post(self):
        Contact.objects.create(**self.validated_data)

class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields='__all__'

class RegisterSerializer(serializers.ModelSerializer):
    class Meta:
        model = Register_Table
        fields='__all__'

class SongSerializer(serializers.ModelSerializer):
    song_category = CategorySerializer()
    uploaded_by = RegisterSerializer()
    class Meta:
        model = Song
        fields=['id','song_name','artist_name','song_file','uploaded_on','uploaded_by','song_category']