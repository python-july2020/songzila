"""myproject URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path,include
from myapp import views

from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', include('apis.urls')),
    path('cbv/',include('class_based_app.urls')),
    path('', views.homepage, name="homepage"),
    path('about/', views.aboutpage, name="aboutpage"),
    path('contact/', views.contact_us, name="contactpage"),
    path('login/', views.loginpage, name="loginpage"),
    path('register/', views.registerpage, name="registerpage"),
    path('dashboard/', views.dashboard, name="dashboard"),
    path('user_logout/', views.user_logout, name="user_logout"),
    path('check_if_user_exist/', views.check_if_user_exist, name="check_if_user_exist"),
    path('sendemail/', views.send_email_to_user, name="send_email_to_user"),
    path('category_songs/', views.category_songs, name="category_songs"),
    path('sample_django_form/', views.sample_django_form, name="sample_django_form"),
    path('upload_song/', views.upload_song, name="upload_song"),
    path('mysongs/', views.mysongs, name="mysongs"),
    path('change_password', views.change_password, name="change_password"),
    path('change_password_ajax', views.change_password_ajax, name="change_password_ajax"),
    path('search_song', views.search_song, name="search_song"),
    
    path("donate/", views.donation, name="donate"),
    path("success/", views.Success_Payment, name="success"),

]+static(settings.MEDIA_URL, document_root = settings.MEDIA_ROOT)

