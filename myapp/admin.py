from django.contrib import admin
from myapp.models import Contact, Register_Table, Category, Song, donors
# Register your models here.

admin.site.site_header = "Test Project"

class ContactAdmin(admin.ModelAdmin):
    # fields = ["name","subject"]
    list_display = ["id","name","subject","received_on"]
    list_filter = ["name","received_on"]
    list_editable = ["name",]
    search_fields = ["name","received_on","subject"]

admin.site.register(Contact, ContactAdmin)
admin.site.register(Register_Table)
admin.site.register(Category)
admin.site.register(Song)
admin.site.register(donors)
