from django import forms 
from django.core import validators
from myapp.models import Song

class FeedBackForm(forms.Form):
    full_name = forms.CharField(max_length=20)
    email = forms.EmailField()
    password = forms.CharField(widget=forms.PasswordInput())
    age = forms.IntegerField(min_value=15, max_value=80)
    satisfied = forms.BooleanField(required=False)
    message = forms.CharField(widget=forms.Textarea(attrs={"class":"form-control"}), validators=[validators.MinLengthValidator(2)])
    dob = forms.DateField(required=False, label="Date Of Birth", widget=forms.DateInput(attrs={"type":"date","style":"background:red;color:white"}))

    
    def clean(self):
        name = self.cleaned_data["full_name"]
        if name not in ["Aman","Admin"]:
            raise forms.ValidationError("You are not allowed to submit data")    

class SongForm(forms.ModelForm):
    class Meta:
        model = Song 
        exclude = ["uploaded_by"]
        # fields = ["song_name", "song_file"]
        # fields = '__all__'
