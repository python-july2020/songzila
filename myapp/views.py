from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from myapp.models import Contact, Register_Table, Category, Song, donors
from django.contrib.auth.models import User
from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.decorators import login_required
from django.core.mail import EmailMessage
from django.db.models import Q
from payTm import checksum
from django.views.decorators.csrf import csrf_exempt
# Create your views here.

def contact_us(request):
    context = {}
    
    # Retrive/Fetch all data from Contact table
    all_data = Contact.objects.all().order_by("-id")
    # all_data = Contact.objects.filter(name__contains="Am")
    context["previous_feedback"] = all_data

    if request.method == "POST":

        # Get values from HTML form by using name attribute
        nm = request.POST["full_name"]
        email = request.POST["email"]
        sub = request.POST["subject"]
        msz = request.POST.get("message")

        #Save data to database (Contact Model)
        obj = Contact(name=nm, email=email, subject=sub, message=msz)
        obj.save()
        context["status"] = "Dear Mr./Ms. {}, feedback sent successfully, Thanks for your time!".format(nm)
        # return HttpResponse("<h1>Name:{}<br>Email:{}<br>Subject:{}<br>Message:{}</h1>".format(nm,email,sub,msz))
    
    return render(request, "contact.html", context)

def homepage(request):
    context={}
    cats = Category.objects.all()
    context["cats"] = cats

    # recent_songs = Song.objects.filter(artist_name="Karan Aujla")
    # recent_songs = Song.objects.filter(song_name__contains="a")
    recent_songs = Song.objects.all().order_by("-id")[:4]
   
    context["songs"] = recent_songs

    return render(request,"home.html", context)

def aboutpage(request):
    return render(request, "about.html")

def loginpage(request):
    context={}
    if request.method=="POST":
        if "signinbtn" in request.POST:
            un = request.POST.get("username")
            pwd = request.POST.get("password")
            check_user = authenticate(username=un, password=pwd)
 
            if check_user:
                login(request, check_user)
                return HttpResponseRedirect("/dashboard/")
            else:
                context["error"] = "Invalid Login Details"
    return render(request, "login.html", context)

def registerpage(request):
    context = {}
    if request.method=="POST":
        first = request.POST.get("first_name")
        last = request.POST.get("last_name")
        un = request.POST.get("username")
        email = request.POST.get("email")
        password = request.POST.get("password")
        gen = request.POST.get("gender")
        
        # Check if user already exist or not
        data = User.objects.filter(username=un)
        if len(data)>0:
            context["status"] = "A user with this username already exist!"
        else: 
            usr = User.objects.create_user(un,email,password)
            usr.first_name = first
            usr.last_name = last 
            # usr.is_staff=True
            usr.save()

            profile = Register_Table(user=usr, gender=gen)
            profile.save()
            context["status"] = "Registeration Successfull!!!"

    return render(request, "register.html", context)

# @login_required
def dashboard(request):
    context={}
    # logged_in_user = get_object_or_404(Register_Table, user__username=request.user.username)
    ## OR # 
    logged_in_user = Register_Table.objects.get(user__username=request.user.username)
    context["profile"] = logged_in_user

    if request.method=="POST":
        
        first = request.POST.get("fname")
        last = request.POST.get("lname")
        contact = request.POST.get("phone")
        gender = request.POST.get("gen")

        ### Update record of 'User' table
        logged_in_user.user.first_name = first 
        logged_in_user.user.last_name = last 
        logged_in_user.user.save()

        ### Update record of 'Register_Table' table
        logged_in_user.phone_number = contact
        logged_in_user.gender = gender

        if "photo" in request.FILES:
            photo = request.FILES["photo"]
            logged_in_user.profile_pic = photo
            
        logged_in_user.save()

        
        context["status"] = "Changes Saved Successfully!!!"

    return render(request, "dashboard.html",context)

# filter() = [{"name":"Aman","rno":1},{}]
# get() = {"name":"Aman","rno":1}

def user_logout(request):
    logout(request)
    return HttpResponseRedirect("/")


def check_if_user_exist(request):
    usern = request.GET.get("user_name")
    print("USERNAME =", usern)
    data = User.objects.filter(username=usern)
    if len(data) > 0:
        return HttpResponse(1)
    else:
        return HttpResponse(0)


def send_email_to_user(request):
    context = {}
    if request.method=="POST":
        to_email = request.POST.get('to').split(",")
        subject = request.POST.get('sub')
        msz = request.POST.get('message')
        
        try:
            obj = EmailMessage(subject, msz, to=to_email)
            obj.send()
            context.update({"status":"Email Sent Successfully","color":"alert-success"})
        except:
            context.update({"status":"Could not Send Email!","color":"alert-danger"})
   

    return render(request, "send_email.html", context)

def category_songs(request):
    context = {}
    if "category_id" in request.GET:
        cid = request.GET.get("category_id")
        try:
            all_songs = Song.objects.filter(song_category__id=cid)
            context["songs"] = all_songs
        except:
            return HttpResponse("OOPS Something Went Wrong!")

    return render(request,"category_songs.html", context)

### SAMPLE DJANGO FORM
from myapp.forms import FeedBackForm, SongForm

def sample_django_form(request):
    context={}
    form = FeedBackForm()
    context["myform"] = form
    if request.method=="POST":
        data = FeedBackForm(request.POST)
        print(data)
        ### Check if data is valid or not 
        if data.is_valid():
            name = data.cleaned_data['full_name']
            return HttpResponse("<h1>Welcome "+name+"</h1>")
        else:
            context["errors"]=data.errors

    return render(request,"sample_django_form.html", context)

def upload_song(request):
    context = {}
    form = SongForm()
    context["upload_form"] = form

    logged_in_user = Register_Table.objects.get(user__id=request.user.id)

    if request.method == "POST":
        form = SongForm(request.POST, request.FILES)
        if form.is_valid():
            data = form.save()
            data.uploaded_by = logged_in_user   
            data.save()

            context["status"] = "Song added successfully!"
        else:
            context["status"] = form.errors
            
    return render(request, "upload_song.html", context)

def mysongs(request):
    context = {}
    if "q" in request.GET:
        try:
            song_id = request.GET.get("q")
            get_object_or_404(Song, pk=song_id).delete()
            context["message"] = "Song deleted Successfully"
        except:
            context["message"] = "Could not delete, please try again!"

    mysongs_all = Song.objects.filter(uploaded_by__user__id = request.user.id)
    context["mysongs"] = mysongs_all
    
    return render(request, "mysongs.html", context)

def change_password(request):
    return render(request, "change_password.html")

def change_password_ajax(request):
    curr_pass = request.POST.get("currentpassword")
    new_password = request.POST.get("newpassword")

    logged_in_user = get_object_or_404(User, pk=request.user.id)
    matched = logged_in_user.check_password(curr_pass)
    if matched:
        logged_in_user.set_password(new_password)
        logged_in_user.save()
        # To login again after change password
        login(request, logged_in_user)
        return JsonResponse({"status":"success"})
    else:
        return JsonResponse({"status":"failed"})

def search_song(request):
    query = request.GET.get("q")

    # data = list(Song.objects.filter(song_name__contains=query).values())
    # data = (Song.objects.filter(song_name__contains=query)&Song.objects.filter(artist_name__contains=query))
    # data = list((Song.objects.filter(song_name__contains=query)|Song.objects.filter(artist_name__contains=query)).values())
    
    data = list(Song.objects.filter(Q(song_name__contains = query)|Q(artist_name__contains=query)).values())
    return JsonResponse({"status":"success","songs":data})

def donation(request):
    all_donors = donors.objects.filter(status=True)
    if request.method=="POST":
        name = request.POST.get("fname")
        con = request.POST.get("con")
        email = request.POST.get("email")
        amt = request.POST.get("amt")
        des = request.POST.get("desc")

        obj = donors(name=name,contact=con,amount=amt, description=des, email=email)
        obj.save()

        data_dict = {
            'MID':'YOUR_MID',
            'ORDER_ID':str(obj.id),
            'TXN_AMOUNT':str(amt),
            'CUST_ID':str(email),
            'INDUSTRY_TYPE_ID':'Retail',
            'WEBSITE':'WEBSTAGING',
            'CHANNEL_ID':'WEB',
	        # 'CALLBACK_URL':'http://songzila.pythonanywhere.com/success/',
	        'CALLBACK_URL':'http://127.0.0.1:8000/success/',
        }
        data_dict["CHECKSUMHASH"] = checksum.generate_checksum(data_dict, "YOUR_MERCHENT_KEY")
        return render(request, "process_payment.html",{"details":data_dict,})

    return render(request, "donate.html",{"all_donors":all_donors})

@csrf_exempt
def Success_Payment(request):
    print(request.POST)
    context = {}
    if request.POST["STATUS"] == "TXN_SUCCESS":
        don = donors.objects.get(id=request.POST["ORDERID"])
        don.status = True
        don.save()
        context.update({"status":"success","msz":"Tracation Successfull!"})
        
    else:
        context.update({"status":"danger","msz":"Tracation Failed! because: {}".format(request.POST["RESPMSG"])})
    return render(request, "success_payment.html",context)

