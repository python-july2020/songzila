from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import TemplateView, ListView, DetailView, CreateView,DeleteView
from myapp.models import Song, Contact, Category
# Create your views here.

class ContactView(TemplateView):
    template_name = "cbv_files/cbv_contact.html"
    
    def get_context_data(self, *args, **kwargs):
        context = super(ContactView,self).get_context_data(*args, **kwargs)
        context["greet"] = "Good Morning to All!"
        context["songs"] = Song.objects.all()
        return context

class CategoryList(ListView):
    model = Category
    # queryset = Contact.objects.filter(name__contains="Am")
    # queryset = Contact.objects.filter(subject="Greetings")
    context_object_name = "categories"


class CategoryDetail(DetailView):
    model = Category
    context_object_name = "single_category"

class CategoryCreate(CreateView):
    model = Category
    fields = ["name"]
    context_object_name = "create"

class CategoryDelete(DeleteView):
    model = Category
    success_url = reverse_lazy("class_based_app:category_list")