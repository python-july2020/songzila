from django.apps import AppConfig


class ClassBasedAppConfig(AppConfig):
    name = 'class_based_app'
